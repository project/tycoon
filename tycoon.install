<?php
/**
 * @file tycoon.install
 * Contains install and update functions for Tycoon.
 */
 
/**
 * Implementation of hook_install().
 */
function tycoon_install() {
  
}

/**
 * Implementation of hook_uninstall().
 */
function tycoon_uninstall() {
  
}

/**
 * Implementation of hook_schema().
 *
 * Generate the current version of the database schema from
 * the sequence of schema update functions. Uses a similar
 * method to install.inc's drupal_get_schema_versions() to
 * establish the update sequence.
 *
 * To change the schema, add a new tycoon_schema_N()
 * function to match the associated tycoon_update_N()
 */
function tycoon_schema($caller_function = FALSE) {
  static $get_current;
  static $schemas = array();

  // If called with no arguments, get the latest version of the schema.
  if (!isset($get_current)) {
    $get_current = $caller_function ? FALSE : TRUE;
  }

  // Generate a sorted list of available schema update functions.
  if ($get_current || empty($schemas)) {
    $get_current = FALSE;
    $functions = get_defined_functions();
    foreach ($functions['user'] as $function) {
      if (strpos($function, 'tycoon_schema_') === 0) {
        $version = substr($function, strlen('tycoon_schema_'));
        if (is_numeric($version)) {
          $schemas[] = $version;
        }
      }
    }
    if ($schemas) {
      sort($schemas, SORT_NUMERIC);

      // If a specific version was requested, drop any later
      // updates from the sequence.
      if ($caller_function) {
        do {
          $schema = array_pop($schemas);
        } while ($schemas && $caller_function != 'tycoon_schema_'. $schema);
      }
    }
  }

  // Call tycoon_schema_<n>, for the highest available <n>.
  if ($schema = array_pop($schemas)) {
    $function = 'tycoon_schema_'. $schema;
    return $function();
  }

  return array();
}

/**
 * Tycoon 2's initial schema.
 */
function tycoon_schema_7000() {
  $schema['tycoon_pay_method_options'] = array(
    'description' => 'Stores tycoon pay method options.',
    'fields' => array(
      'pmid' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
        'default' => '',
        'description' => 'The identifier for this pay method.',
      ),
      'data' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
        'description' => 'A serialized array of options associated with this tycoon object.',
      ),
    ),
    'primary key' => array('pmid'),
  );
  
  $schema['tycoon_transaction'] = array(
    'description' => 'Stores transaction data for a tycoon transaction.',
    'fields' => array(
      'txid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique transaction ID.',
      ),
      'state' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
        'default' => '',
        'description' => 'The current state of the transaction.',
      ),
      'total' => array(
        'type' => 'float',
        'precision' => '10',
        'scale' => '2',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The total value of the transaction.',
      ),
      'total_paid' => array(
        'type' => 'float',
        'precision' => '10',
        'scale' => '2',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The amount paid of the transaction total.',
      ),
      'currency' => array(
        'type' => 'varchar',
        'length' => '3',
        'not null' => FALSE,
        'description' => 'The type of currency of this transaction.',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "User's {users}.uid.",
      ),
      'mail' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
        'default' => '',
        'description' => 'The email address of the customer.',
      ),
      'first_name' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The first name of the customer.',
      ),
      'last_name' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The last name of the customer.',
      ),
      'phone' => array(
        'type' => 'varchar',
        'length' => 25,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The phone number of the customer.',
      ),
      'company' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The company at the delivery location.',
      ),
      'street1' => array(
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The street address of the customer.',
      ),
      'street2' => array(
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The second line of the street address.',
      ),
      'city' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The city of the customer.',
      ),
      // Should this be converted to varchar?
      'province' => array(
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The state/zone/province id of the customer.',
      ),
      'postal_code' => array(
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The postal code of the customer.',
      ),
      'country' => array(
        'type' => 'varchar',
        'length' => 2,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The country ID of the customer.',
      ),
      'created' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp when this transaction was created.',
      ),
      'updated' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp when this transaction was last updated.',
      ),
      'data' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
        'description' => 'A serialized array of data associated with this transaction.',
      ),
    ),
    'primary key' => array('txid'),
    'indexes' => array(
      'uid' => array('uid'),
      'mail' => array('mail'),
      'created' => array('created'),
      'updated' => array('updated'),
    ),
  );
  $schema['tycoon_activity'] = array(
    'description' => 'Stores activity data for a tycoon transaction.',
    'fields' => array(
      'aid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique activity ID.',
      ),
      'txid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "Transactions's {tycoon_transaction}.txid.",
      ),
      'action' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
        'default' => '',
        'description' => 'The action taken.',
      ),
      'message' => array(
        'type' => 'text',
        'description' => 'The line item message.',
      ),
      'args' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
        'description' => 'A serialized array of arguments for the message.',
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Whether the activity was a success or failure.',
      ),
      'created' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The Unix timestamp when this action was created.',
      ),
      'data' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
        'description' => 'A serialized array of data associated with this action.',
      ),
    ),
    'primary key' => array('aid'),
    'indexes' => array(
      'txid' => array('txid'),
      'action' => array('action'),
      'created' => array('created'),
    ),
    'foreign keys' => array(
      'activity_transaction' => array(
        'table' => 'tycoon_transaction',
        'columns' => array('txid' => 'txid'),
      ),
    ),
  );
  $schema['tycoon_line_item'] = array(
    'description' => 'Stores line item data for a tycoon transaction.',
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary Key: Unique line item ID.',
      ),
      'txid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "Transactions's {tycoon_transaction}.txid.",
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 31,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The line item label.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The line item description.',
      ),
      'qty' => array(
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The quantity of the line item.',
      ),
      'price' => array(
        'type' => 'numeric',
        'precision' => 16,
        'scale' => 5,
        'not null' => TRUE,
        'default' => 0.0,
        'description' => 'The price per unit of the line item in the default currency.',
      ),
      'taxable' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Whether the amount of the line item is taxable.',
      ),
      'weight' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The sort criteria of the line item.',
      ),
      'data' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
        'description' => 'A serialized array of data associated with this line item.',
      ),
    ),
    'primary key' => array('lid'),
    'indexes' => array(
      'txid' => array('txid'),
      'weight' => array('weight'),
    ),
    'foreign keys' => array(
      'line_item_transaction' => array(
        'table' => 'tycoon_transaction',
        'columns' => array('txid' => 'txid'),
      ),
    ),
  );
  return $schema;
}
