<?php
/**
 * Tycoon hooks and API functions
 */
 
/**
 * Implements hook_tycoon_action_info().
 */
function tycoon_tycoon_action_info() {
  $actions = array(
    'validate' => array(
      'label' => t('Validate'),
      'handler' => 'TycoonActionValidate',
      'states' => array('pending', 'canceled', 'refunded'),
      'finished' => 'validated',
    ),
    'process' => array(
      'label' => t('Process'),
      'handler' => 'TycoonActionProcess',
      'states' => array('validated'),
      'finished' => 'complete'
    ),
    'refund' => array(
      'label' => t('Refund'),
      'handler' => 'TycoonActionRefund',
      'states' => array('complete'),
      'finished' => 'refunded',
    ),
  );
  return $actions;
}
