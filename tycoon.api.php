<?php
/**
 * @file
 * Hooks provided by the Tycoon Payment API module.
 */

/**
 * Inform Payment API about one or more pay methods.
 *
 * @return
 *   An array whose keys are pay method names and whose values identify
 *   properties of those methods:
 *   - label: The human-readable name of the pay method.
 *   - description:
 *   - controller class: The name of the class that is used to load the object.
 *     The class has to implement the TycoonPayMethodInterface interface.
 *   - file: 
 *   - file path: 
 *
 * @see hook_tycoon_pay_method_info_alter()
 */
function hook_tycoon_pay_method_info() {
  $info = array(
    'null_pay' => array(
      'label' => t('Null Pay'),
      'controller class' => 'NullPay',
    ),
  );
  return $info;
}

function hook_tycoon_pay_method_info_alter($info) {
  
}

/**
 * Add transaction actions.
 *
 * @return
 *   An array of actions. Each operation is an associative array that may
 *   contain the following key-value pairs:
 *   - 'label': Required. The label for the operation.
 *   - 'callback': Required. The function to call for the action.
 *     The callback should return an array to be added to the activity log or FALSE if there was an error.
 *   - 'arguments': Optional. An array of additional arguments to pass
 *     to the callback function.
 */
function hook_tycoon_action_info() {
  $actions = array(
    'process' => array(
      'label' => t('Process'),
      'handler' => 'TycoonProcess',
      'arguments' => array('op' => 'foo'),
      'states' => array('created', 'refunded'),
      'finished' => 'completed'
    ),
  );
  return $actions;
}

/**
 * Alter transaction actions.
 */
function hook_tycoon_action_info_alter(&$actions) {
  $actions['process']['label'] = t('Process Order');
}

/**
 * React to an action happening on a transaction.
 * 
 * @param $op
 *   The operation that is being performed.
 * @param $transaction
 *   The transaction the activity is being logged to.
 * @param $activity
 *   The activity handler object.
 */
function hook_tycoon_activity($op, $transaction, &$activity) {
  
}

/**
 * Prepare an action for execution.
 * 
 * @param $action
 *   The action that is being performed
 * @param $transaction
 *   The transaction the action is being performed on.
 * @param $args
 *   Optional arguments that an action may or may not pass.
 */
function hook_tycoon_action_prepare($action, $transaction, &$arguments = array()) {
  
}

/**
 * React to an action executing.
 * 
 * @param $action
 *   The action that is being performed
 * @param $transaction
 *   The transaction the action is being performed on.
 * @param $object
 *   The action object.
 */
function hook_tycoon_action_execute($action, $transaction, &$object) {
  
}

/**
 * Act on transactions being loaded from the database.
 */
function hook_tycoon_transaction_load(&$transaction) {
  
}

/**
 * Respond to creation of a new transaction.
 */
function hook_tycoon_transaction_create(&$transaction) {
  
}

/**
 * Respond to updates to a transaction.
 * 
 * @param $transaction
 *   The transaction object
 * @param $op
 *   The operation that is causing the update
 */
function hook_tycoon_transaction_update(&$transaction, $op) {
  switch ($op) {
    case 'save':
      //code goes here
      break;
    case 'activity':
      //an activity was added, react here
      break;
  }
}

/**
 * Respond to transaction deletion.
 */
function hook_tycoon_transaction_delete(&$transaction) {
  
}
