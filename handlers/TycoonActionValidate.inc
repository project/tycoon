<?php
/**
 * This class executes the pay method's validation action on a transaction.
 */
class TycoonActionValidate extends TycoonActionHandler {
  
  protected $key = 'validate';
  
  /**
   * Executes the Pay Methods
   */
  function execute(&$args) {
    try {
      $pay_method = $this->txn->pay_method();
      $data = $pay_method->validate($this->txn, $args);
    
      $this->set_status(TYCOON_ACTION_SUCCESS);
      $activity = array('message' => 'Valididation successful.', 'data' => $data);
      $aid = $this->log_activity($activity + array('args' => array(), 'data' => array()));
    }
    catch (TycoonActionException $e) {
      $this->set_errors($e->errors);
      $aid = $this->log_activity($e->activity());
    }
    // Would we save here?
    //$pay_method->save($aid, $data);
    $this->aid = $aid;
    return $aid;
  }
}
