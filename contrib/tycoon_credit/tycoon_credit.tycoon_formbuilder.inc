<?php

/**
 * Implements hook_tycoon_formbuilder_elements().
 */
function tycoon_credit_tycoon_formbuilder_elements() {
  return array(
    'cc_number' => array(
      '#type' => 'textfield',
      '#title' => t('Credit Card Number'),
      '#maxlength' => 255,
      '#size' => 16,
      '#required' => TRUE,
    ),
    'cc_date' => array(
      '#type' => 'value', // select
      '#title' => t('Expiration Date'),
      '#required' => TRUE,
      '#process' => array('tycoon_credit_element_cc_date'),
      '#element_validate' => array('tycoon_credit_element_cc_date_validate'),
      '#theme_wrappers' => array('form_element'),
    ),
    'cc_cvv' => array(  
      '#type' => 'textfield',
      '#title' => t('CVV Number'),
      '#maxlength' => 4,
      '#size' => 4,
      '#required' => TRUE,
    ),
  );
}

function tycoon_credit_element_cc_date($element, &$form_state, $form) {
  $options = array();
  foreach (range(1, 12) as $month) {
    $options[sprintf("%02d", $month)] = sprintf("%02d", $month);
  }
  $element['month'] = array(
    '#type' => 'select',
    '#title' => t('Month'),
    '#options' => $options,
    '#value' => empty($element['#value']) ? NULL : $element['#value']['month'],
    '#default_value' => '01',
    '#required' => TRUE,
  );
  
  $year = date('Y');
  $options = array();
  foreach (range($year, $year + 20) as $year) {
    $options[$year] = $year;
  }
  $element['year'] = array(
    '#type' => 'select',
    '#title' => t('Year'),
    '#options' => $options,
    '#value' => empty($element['#value']) ? NULL : $element['#value']['year'],
    '#default_value' => $year,
    '#required' => TRUE,
  );
  $element['#tree'] = TRUE;
  return $element;
}

function tycoon_credit_element_cc_date_validate($element, &$element_state) {
  $month = $element['#value']['month'];
  $year = $element['#value']['year'];
  
  if ($year && $month) {
    if ($year < date('Y') || ($year == date('Y') && $month < date('m'))) {
      form_error($element, t('The credit card appears to be expired.'));
    }
  }
  
  form_set_value($element['month'], NULL, $element_state);
  form_set_value($element['year'], NULL, $element_state);
  // in the format MMYYYY
  form_set_value($element, sprintf("%02d", $month) . $year, $element_state);

  return $element;
}
