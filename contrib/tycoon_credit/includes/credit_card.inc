<?php

/**
 * An object that represents a pay method.
 */
class TycoonCreditCardPayMethod extends TycoonPayMethod {
  
  function validate(&$transaction, &$data) {
    try {
      parent::validate($transaction, $data);
      
      if (isset($data['address'])) {
        // Set transaction data for address fields
        foreach ($data['address'] as $key => $value) {
          $transaction->$key = isset($data[$key]) ? $data[$key] : NULL;
        }
      }
      
      $errors = array();
      if (empty($transaction->total) || !is_numeric($transaction->total)) {
        $errors[] = array(
          'message' => 'Transaction total must have a value.',
          'data' => array(
            'keys' => array('cc_number'),
          ),
        );
      }
      
      $error = TycoonCreditCardPayMethod::cc_number_validate($data['cc_number']);
      if ($error !== TRUE) {
        $errors[] = $error + array(
          'data' => array(
            'keys' => array('cc_number'),
          ),
        );
      }
      
      $error = TycoonCreditCardPayMethod::cc_date_validate($data['cc_date']);
      if ($error !== TRUE) {
        $errors[] = $error + array(
          'data' => array(
            'keys' => array('cc_date'),
          ),
        );
      }
      
      $info = TycoonCreditCardPayMethod::cc_number_info($data['cc_number']);
      if (!empty($info['type'])) {
        if (($error = TycoonCreditCardPayMethod::cc_cvv_validate($data['cc_cvv'], $info['type'])) !== TRUE) {
          $errors[] = $error + array(
            'data' => array(
              'keys' => array('cc_cvv'),
            ),
          );
        }
      }
      
      if (!empty($errors)) {
        throw new TycoonActionException('Credit card transaction failed.', array(), $errors);
      }
    }
    catch (TycoonActionException $e) {
      throw new TycoonActionException('Credit card transaction failed.', $e->args, $e->errors, $e->getCode());
    }
    return TRUE;
  }

  function process(&$transaction, &$data) {
    try {
      parent::process($transaction, $data);
      
      $info = TycoonCreditCardPayMethod::cc_number_info($data['cc_number']);
      $data = TycoonCreditCardPayMethod::clean_data($data);
      
      $data['type'] = $info['type'];
      $data['mask'] = $info['substring'];
      $data['data'] = $info;
    }
    catch (TycoonActionException $e) {
      throw new TycoonActionException('Credit card transaction failed.', $e->args, $e->errors, $e->getCode());
    }  
    return $data;
  }
  
  function save($aid, $data) {
    $tycoon_credit = array(
      'aid' => $aid,
      'cc_number' => isset($data['mask']) ? substr($data['mask'], -4) : '',
      'type' => isset($data['type']) ? $data['type'] : '',
      'data' => isset($data['data']) ? $data['data'] : '',
    );
    
    drupal_write_record('tycoon_credit', $tycoon_credit);
  }

  function schema() {
    return array(
      'first_name' => array(
        'type' => 'varchar',
        'title' => t('First name'),
        'required' => TRUE,
        'maxlength' => 50,
      ),
      'last_name' => array(
        'type' => 'varchar',
        'title' => t('Last name'),
        'required' => TRUE,
        'maxlength' => 50,
      ),
      'mail' => array(
        'type' => 'varchar',
        'title' => t('Email'),
        'required' => TRUE,
        'maxlength' => 50,
      ),      
      'company' => array(
        'type' => 'varchar',
        'title' => t('Company'),
        'maxlength' => 50,
      ),
      'address' => array(
        'type' => 'address',
        'title' => t('Address'),
        'required' => TRUE,
      ),
      'phone' => array(
        'type' => 'phone',
        'title' => t('Phone number'),
        'maxlength' => 25,
      ),
      
      'cc_number' => array(
        'type' => 'cc_number',
        'title' => t('Credit Card Number'),
        'required' => TRUE,
      ),
      'cc_date' => array(
        'type' => 'cc_date',
        'title' => t('Expiration Date'),
        'required' => TRUE,
      ),
      'cc_cvv' => array(
        'type' => 'cc_cvv',
        'title' => t('CVV Number'),
        'required' => TRUE,
      ),
    );
  }
  
  static function cc_number_validate($cc_number) {
    $info = TycoonCreditCardPayMethod::cc_number_info($cc_number);
    if ($info['status'] == 'invalid') {
      return array(
        'message' => 'Invalid credit card number. (Reason code: %failure)',
        'args' => array(
          '%failure' => $info['failure'],
        ),
      );
    }
    return TRUE;
  }
  
  static function cc_number_info($cc_number) {
    static $validated = array();
    if (empty($validated[$cc_number])) {
      require_once 'creditcards.class.inc';
      $ccv = new CreditCardValidator();
      $ccv->Validate($cc_number);
      $validated[$cc_number] = $ccv->GetCardInfo();
    }
    return $validated[$cc_number];
  }
  
  /**
   * Validates a credit card expiration date in the following formats.
   * 
   * MMYY, MM/YY, MM-YY, MMYYYY, MM/YYYY, MM-YYYY,
   * 
   * If the value passed in is not in one of the above numeric date formast it will fail the regex
   * 
   * The $parts array will have the month at $parts[2][0] and the 2 digit year at $parts[8][0]
   * 
   * The regex checks to ensure that the month is a valid month i.e. is 1-12 with single digit months requiring a padding 0
   * It disallows months over the value of 12 and the special case of 00
   * /^((((0+(?=([1-9])))|(1+(?=([0-2])))){1}[0-9]{1})[-|\/]?((20){0,1}([0-9]{2})))$/i
   * 
   */
  static function cc_date_validate($cc_date) {
    $regex = '/^((((0+(?=([1-9])))|(1+(?=([0-2])))){1}[0-9]{1})[-|\/]?((20){0,1}([0-9]{2})))$/i';
    $parts = array();
    if (preg_match_all($regex, $cc_date, $parts)) {
      $month = $parts[2][0];
      $year = $parts[8][0];
      if ($year && $month) {
        if ($year > date('Y') || ($year == date('Y') && $month >= date('m'))) {
          return TRUE;
        }
      }
    }
    
    return array(
      'message' => 'The credit card appears to be expired.',
    );
  }

  static function cc_cvv_validate($cc_cvv, $cc_type) {
    if ($cc_type == 'amex') {
      if (!is_numeric($cc_cvv) || strlen($cc_cvv) != 4) {
        return array(
          'message' => 'The security code must be 4 numeric digits for American Express cards.',
        );
      }
    }
    elseif (!is_numeric($cc_cvv) || strlen($cc_cvv) != 3) {
      return array(
        'message' => 'The security code must be 3 numeric digits.',
      );
    }
    return TRUE;
  }

  static function payment_types() {
    $payment_types = array(
      'visa' => t('Visa'),
      'mc'   => t('Mastercard'),
      'amex' => t('American Express'),
      'discover' => t('Discover'),
    );
    return $payment_types;
  }
  
  /**
   * Clean up data for storage.
   */
  static function clean_data($data) {
    unset($data['cc_date'], $data['cc_cvv'], $data['cc_number']);
    return $data;
  }
  
  static function mask_cc_number($cc_number) {
    if (strlen($data['cc_number']) > 4) {
      return str_repeat('*', strlen($cc_number) - 4) . substr($cc_number, -4);
    }
    else {
      return str_repeat('*', 12) . $cc_number;
    }
  }
  
  static function token_type_info() {
    return array(
      'name' => t('Credit Card'),
      'description' => t('Tokens related to Tycoon Credit Card Pay Method.'),
      'needs-data' => 'tycoon_credit_card',
    );
  }
  
  static function token_token_info() {
    return array(
      'cc_number' => array(
        'name' => t("Credit Card (Last 4 Digits)"),
        'description' => t("The action of this activity."),
      ),
      'type' => array(
        'name' => t("Credit Card Type"),
        'description' => t("The message for this activity."),
      ),  
      'data' => array(
        'name' => t("Data"),
        'description' => t("The arguements for this activity."),
      ),          
    );
  }
  
  function tokens($tokens, $options = array()) {
    $url_options = array('absolute' => TRUE);
    if (isset($options['language'])) {
      $url_options['language'] = $options['language'];
      $language_code = $options['language']->language;
    }
    else {
      $language_code = NULL;
    }
    $sanitize = !empty($options['sanitize']);

    $replacements = array();
        
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'cc_number':
          $replacements[$original] = $this->cc_number;
          break;
        case 'type':
          $replacements[$original] = $this->type;
          break; 
        case 'data':
          $replacements[$original] = $this->data;
          break; 
                                                                                     
      }
    }
    
    return $replacements;
  }
}