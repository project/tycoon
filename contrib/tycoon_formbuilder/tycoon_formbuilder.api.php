<?php
/**
 * Form builder API definitions.
 */

/**
 * Provides basic form info to the form builder
 * 
 * @return
 *   A valid Form API array.
 */
function hook_tycoon_pay_form_fields() {
  $form['foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#required' => TRUE,
  );
  
  return $form;
}

/**
 * Provides some basic info to the form builder about how to structure your form array
 */
function hook_tycoon_pay_form_info() {
  return array('handler' => 'FooBarHandler');
}
