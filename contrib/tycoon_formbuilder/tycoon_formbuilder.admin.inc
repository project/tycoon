<?php

function tycoon_formbuilder_sample_form($form, $form_state) {
  $line_items = array(
    array(
      'type' => 'item',
      'title' => 'Product 1',
      'price' => 3,
      'qty' => 2,
      'description' => 'This is product 1',
      'weight' => 1,
    ),
    array(
      'type' => 'item',
      'title' => 'Product 2',
      'price' => 4,
      'description' => 'This is product 2',
    ),
  );
  
  // TODO: allow changing of pay method
  $pay_method = tycoon_get_default_pay_method();
  $form = tycoon_formbuilder_pay_form($form, $form_state, 10.0875, $line_items, $pay_method['id']);
  
  $form['pay_method']['description'] = array(
    '#type' => 'value',
    '#value' => 'Tycoon form builder module test transaction.',
  );
  
  if (!empty($pay_method)) {
    $form['markup'] = array(
      '#markup' => t('Using %pay_method pay method.', array('%pay_method' => $pay_method['label'])),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#weight' => -1,
    );
  }
  
  $form['#submit'][] = 'tycoon_formbuilder_sample_form_submit';
  
  return $form;
}

function tycoon_formbuilder_sample_form_submit($form, &$form_state) {
  $transaction = $form_state['tycoon_formbuilder']['transaction'];
  // $form_state['redirect'] = '<front>';
  drupal_set_message(t('Test transaction successfull. Transaction ID %id', array('%id' => $transaction->txid)));
}
