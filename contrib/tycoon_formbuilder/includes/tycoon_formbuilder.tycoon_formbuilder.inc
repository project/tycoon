<?php

/**
 * Implements hook_tycoon_formbuilder_elements().
 */
function tycoon_formbuilder_tycoon_formbuilder_elements() {
  return array(
    'varchar' => array(
      '#type' => 'textfield',
      '#size' => 30,
      '#maxlength' => 255,
    ),
    'address' => array(
      '#type' => 'tycoon_address',
      '#title' => t('City'),
      '#maxlength' => 50,
      '#size' => 30,
      '#required' => TRUE,
    ),
    'phone' => array(  
      '#type' => 'textfield',
      '#title' => t('Phone number'),
      '#maxlength' => 25,
      '#size' => 14,
      '#element_validate' => array('tycoon_formbuilder_element_phone_validate'),
    ),
  );
}

function tycoon_formbuilder_element_country($element, &$form_state, $form) {
  module_load_include('inc', 'tycoon_formbuilder', 'includes/field_country');
  
  $element['#empty_value'] = '';
  $element['#options'] = _tycoon_formbuilder_country_options_list();
  return $element;
}

function tycoon_formbuilder_element_postal_validate($element, &$form_state, $complete_form) {
  // TODO
}

function tycoon_formbuilder_element_phone_validate($element, &$form_state, $complete_form) {
  // TODO
}
