<?php

function _tycoon_formbuilder_country_options_list() {
  // Necessary for country_get_list().
  require_once DRUPAL_ROOT . '/includes/locale.inc';

  return country_get_list();
}
