<?php


/**
 * Form builder. Configure annotations.
 */
function tycoon_receipt_admin_settings() {
  $form = array();

  $form['tycoon_receipt_from_email'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('tycoon_receipt_from_email', variable_get('site_mail', ini_get('sendmail_from'))),
    '#title' => t('Email to use as from address on all receipts or notificatoin emails'),
  );  
  
  return system_settings_form($form);
}
