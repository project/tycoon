<?php
/**
 * @file
 * This file is the default transaction complete email template for Tycoon.
 */
?>

<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#006699" style="font-family: verdana, arial, helvetica; font-size: small;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">
        <tr valign="top">
          <td>
            <p><b><?php echo t('Transaction summary for: [tycoon_transaction:full-name]!'); ?></b></p>
            <table cellpadding="4" cellspacing="0" border="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
              <tr>
                <td colspan="2" bgcolor="#006699" style="color: white;">
                  <b><?php echo t('Transaction Information:'); ?></b>
                </td>
              </tr>
              <tr>
                <td nowrap="nowrap">
                  <b><?php echo t('Status:'); ?></b>
                </td>
                <td width="98%">
                  [tycoon_transaction:state]
                </td>
              </tr>
             <tr>
                <td nowrap="nowrap">
                  <b><?php echo t('Name:'); ?></b>
                </td>
                <td width="98%">
                  [tycoon_transaction:full-name]
                </td>
              </tr>              
              
              <tr>
                <td nowrap="nowrap">
                  <b><?php echo t('E-mail Address:'); ?></b>
                </td>
                <td width="98%">
                  [tycoon_transaction:mail]
                </td>
              </tr>
              
              <tr>
                <td nowrap="nowrap">
                  <b><?php echo t('Phone:'); ?></b>
                </td>
                <td width="98%">
                  [tycoon_transaction:phone]
                </td>
              </tr>
              <tr>
                <td colspan="2">

                  <table width="100%" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: small;">
                    <tr>
                      <td valign="top" width="50%">
                        <b><?php echo t('Billing Address:'); ?></b><br />
                        [tycoon_transaction:address]<br />
                      </td>
                    </tr>
                  </table>

                </td>
              </tr>
              <tr>
                <td nowrap="nowrap">
                  <b><?php echo t('Transaction Total Paid:'); ?></b>
                </td>
                <td width="98%">
                  <b>[tycoon_transaction:currency] [tycoon_transaction:total-paid]</b>
                </td>
              </tr>
              <tr>
                <td colspan="2" bgcolor="#006699" style="color: white;">
                  <b><?php echo t('Transaction Summary:'); ?></b>
                </td>
              </tr>
              <tr>
                <td colspan="2">

                  <table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
                    <tr>
                      <td nowrap="nowrap">
                        <b><?php echo t('Transaction #:'); ?></b>
                      </td>
                      <td width="98%">
                        [tycoon_transaction:txid]
                      </td>
                    </tr>

                    <tr>
                      <td nowrap="nowrap">
                        <?php echo t('Transaction Total:'); ?>&nbsp;
                      </td>
                      <td width="98%">
                        [tycoon_transaction:currency] [tycoon_transaction:total]
                      </td>
                    </tr>
                    <tr>
                      <td nowrap="nowrap">
                        <?php echo t('Transaction Line Items:'); ?>&nbsp;
                      </td>
                      <td width="98%">
                        [tycoon_transaction:line-items]
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
