<?php

function tycoon_ui_transaction_title($transaction) {
  return t('Transaction #@txid', array('@txid' => $transaction->txid));
}

function tycoon_ui_transaction_list() {
  $query = db_select('tycoon_transaction', 't');
  $query->fields('t');
  $result = $query->execute();
  
  $transactions = array();
  foreach ($result as $transaction) {
    $transactions[] = l(t('Transaction #@txid', array('@txid' => $transaction->txid)), "admin/config/tycoon/transactions/$transaction->txid");
  }
  
  return theme('item_list', array('items' => $transactions));
}

function tycoon_ui_transaction_view($transaction) {
  return print_r($transaction, 1);
}

function tycoon_ui_transaction_edit($transaction) {
  return print_r($transaction, 1);
}
