<?php
/**
 * @file transaction.inc
 * Provides the TycconTransaction object type and associated methods.
 */

/**
 * An object that contains all data of a single transaction.
 */
class TycoonTransaction extends TycoonDBObject {
  protected $db_table = 'tycoon_transaction';
  protected $db_key = 'txid';
  
  protected $pay_method;
  
  protected $activity = array();
  protected $line_item = array();
  
  protected $state = NULL;
  
  // TODO: Support multiple currencies?
  protected $currency = 'USD';
  
  //The following are just placeholders for the data contained in protected elements. See the explanation for $status below.
  //$status is just an alias of $state, except it allows users to see the current state
  //since that variable is protected. It is not saved or called anywhere except set_state so
  //changing it won't affect the transaction at all as it will always be reset to the $state
  public $status = '';
  public $activities = array();
  public $line_items = array();
  
  /**
   * Constructor
   */
  function __construct($options = array()) {
    parent::init();
    
    foreach ($options as $key => $value) {
      $this->__set($key, $value);
    }
    // Make sure all of our sub objects are arrays.
    foreach ($this->db_objects() as $object) {
      $this->$object = array();
    }
    
    //initialize the transaction state. This may or may not be needed here but I'd rather call it now then later
    $this->state();
  }
  
  /**
   * Magic Setter method
   */
  function __set($name, $value) {
    $this->$name = $value;
  }
  
  /**
   * Magic getter function
   */
  function __get($name) {
    return $this->$name;
  }
  
  /**
   * Returns the complete list of dependent objects in a transaction, for the
   * purpose of initialization and loading/saving to/from the database.
   */
  function db_objects() {
    $obj = array(
      'activity' => 'TycoonActivity',
      'line_item' => 'TycoonLineItem',
    );
    return $obj;
  }
  
  /**
   * Load a transaction from the database based upon txid.
   *
   * This is a static factory method that implements internal caching for
   * transaction objects.
   *
   * @param $txid
   *  The txid of the transaction
   * @param $reset
   *  If TRUE, reset this entry in the load cache.
   * @return transaction
   *   A transaction object or NULL if it was not available.
   */

  static function &load($txid, $reset = FALSE) {
    $cache = &drupal_static(__FUNCTION__, array());
    // We want a NULL value to return TRUE here, so we can't use isset() or empty().
    if (!array_key_exists($txid, $cache) || $reset) {
      $result = db_select('tycoon_transaction', 't')
        ->fields('t')
        ->condition('txid', $txid)
        ->execute();
      if ($result->rowCount() == 0) {
        $cache[$txid] = NULL;
      }
      else {
        $transaction = new TycoonTransaction();
        $transaction->load_row($result->fetchObject());
        // Load all of our subtables.

        foreach ($transaction->db_objects() as $key => $class_name) {
          $table = "tycoon_$key";
          $result = db_select($table, 't')
            ->fields('t')
            ->condition('txid', $transaction->txid)
            //->orderBy('position')
            ->execute();
          foreach ($result as $data) {
            $object = new $class_name(FALSE);
            $object->load_row($data);

            // Because it can get complicated with this much indirection,
            // make a shortcut reference.
            $location = &$transaction->$key;

            // If we have a basic id field, load the item onto the transaction
            // based on this ID, otherwise push it on.
            if (!empty($object->id)) {
              $location[$object->id] = $object;
            }
            else {
              $location[] = $object;
            }
          }
        }
        $transaction->line_items = $transaction->line_item;
        $transaction->activities = $transaction->activity;
        $transaction->loaded = TRUE;
        $cache[$txid] = $transaction;
      }
    }
    
    tycoon_invoke_all('transaction_load', $cache[$txid]);
    return $cache[$txid];
  }
  
  /**
   * Save the transaction to the database. If the transaction does not already
   * exist, a txid will be assigned to the transaction and also returned from
   * this function.
   */
  function save() {
    $hook = 'update';
    $up = 'txid';
    // If we have no txid or our txid is a string, this is a new transaction.
    if (!is_numeric($this->txid)) {
      $up = NULL;
      $hook = 'create';
      $this->created = time();
    }
    
    $this->updated = time();
    $hook = 'transaction_' . $hook;
    tycoon_invoke_all($hook, $this, 'presave');
    
    // If we have no txid this is a new transaction.
    if (!empty($this->txid)) {
      // remove existing table entries
      foreach ($this->db_objects() as $key => $class_name) {
        // TODO: should we make activity conform to the db_objects subtable model?
        if ($key != 'activity') {
          db_delete('tycoon_' . $key)
            ->condition('txid', $this->txid)
            ->execute();
        }
      }
    }
    
    $this->save_row(!empty($this->txid) ? 'txid' : FALSE);
    
    // Save all of our subtables.
    foreach ($this->db_objects() as $key => $class_name) {
      // TODO: should we make activity conform to the db_objects subtable model?
      if ($key != 'activity') {
        $this->_save_rows($key);
      }
    }
    
    tycoon_invoke_all($hook, $this, 'save');
    return $this->txid;
  }
  
  /**
   * Save a row to the database for the given key, which is one of the
   * keys from view::db_objects()
   */
  function _save_rows($key) {
    $count = 0;
    foreach ($this->$key as &$object) {
      $object->txid = $this->txid;
      tycoon_invoke_all($key, 'presave', $this, $object);
      $object->save_row();
      tycoon_invoke_all($key, 'insert', $this, $object);
    }
  }
  
  /**
   * Add a new activity to the transaction, automatically creating an id.
   *
   * @param $aciton
   *   The action taken by the activity.
   * @param $message
   *   The human readable description of the activity;
   * @param $args
   *   An array of arguments associated with the message; optional.
   * @param $data
   *   An array of data associated with the activity; optional.
   * @return
   *   The key to the activity in $transaction->activity, so that the new
   *   activity can be easily accessed.
   */
  function log_activity($args) {
    //set up defaults
    $args += array(
      'args' => array(),
      'data' => array(),
      'status' => 0,
    );
    
    if (empty($args['action']) || empty($args['message'])) {
      return FALSE;
    }

    //Before we do anything save the current state of the txn
    $this->save();
    $args['txid'] = $this->txid;
    
    // Create the new activity object
    //We allow the Activity handler the ability to be overridden, but we are not providing a GUI for this.
    //it is up to a developer to override it if they wish.
    $db_objects = $this->db_objects();
    $handler = $db_objects['activity'];
    $activity = new $handler();
    $activity->options($args);
    
    tycoon_invoke_all('activity', 'presave', $this, $activity);
    
    $aid = $activity->save_row();
    
    tycoon_invoke_all('activity', 'insert', $this, $activity);
    
    // Add the new activity to the transaction.
    $this->activity[$aid] = $activity;
    $this->activities = $this->activity;
    return $aid;
  }
  
  function add_line_item($args) {
    $args += array(
      'title' => '',
      'price' => '',
      'taxable' => FALSE,
      'qty' => 1,
      'description' => '',
      'weight' => 0,
      'data' => NULL,
    );
    if (empty($args['title']) || empty($args['$price']) || empty($args['$qty'])) {
      return FALSE;
    }
    
    // Create the new line item object
    $db_objects = $this->db_objects();
    $handler = $db_objects['line_item'];
    $line_item = new $handler();
    $line_item->options($args);
    
    // Add the new line item object to the transaction.
    tycoon_invoke_all('line_item', 'add', $this, $line_item);
    $this->line_item[] = $line_item;
    $this->update_total();
    $this->line_items = $this->line_item;
    return $line_item;
  }
  
  /**
   * Executes the supplied action on the transaction object
   * 
   * @param $action_name
   *   The name of the action to execute. Must be an action provided in hook_tycoon_action()
   * @param ...
   *   Any additional data to pass to the action
   * 
   * @see hook_tycoon_action_info()
   */
  function do_action($action_name, &$args) {
    //Set the default return
    $return = array('status' => FALSE, 'action' => $action_name);
    
    if ($action = tycoon_get_actions($action_name)) {
      //Checks to make sure the action is valid, the handler exists, and the txn is in the correct state to take this action
      if (class_exists($action['handler']) && in_array($this->state(), $action['states'])) {
        tycoon_invoke_all('action_prepare', $action_name, $this, $args);
        
        $handler = $action['handler'];
        
        //Create and execute the current action
        $object = new $handler($this);
        $activity = $object->execute($args);
        
        //Check the actions status
        if ($object->status() == TYCOON_ACTION_SUCCESS) {
          $this->set_state($action['finished']);
        } 
        
        $this->save();
        
        tycoon_invoke_all('action_execute', $action_name, $this, $object);
        
        $return['activity'] = $this->activity[$activity];
        $return['action'] = $object;
        $return['status'] = $object->status();
        $return['errors'] = $object->errors();
      }
      elseif (!in_array($this->state(), $action['states'])) {
        $return['errors'][] = t('Action, !action, not valid for the current state.', array('!action' => $action['label']));
      }
      else {
        $return['errors'][] = t('Class, !handler, not found for !action action.', array('!handler' => $action['handler'], '!action' => $action['label']));
      }
    }
    else {
      $return['errors'][] = t('Action, !action, not found in the system.', array('!action' => $return['action']));
    }
    return $return;
  }
  
  /**
   * Set the pay method for this transaction.
   */
  function set_pay_method($name = NULL) {
    if (!($this->pay_method = tycoon_get_pay_method($name))) {
      $this->pay_method = tycoon_get_default_pay_method();
    }
    return $this->pay_method;
  }
  
  /**
   * Return the pay method for this transaction.
   */
  function pay_method() {
    if (!$this->pay_method) {
      $this->set_pay_method();
    }
    return $this->pay_method;
  }

  /**
   * A list of possible payment states for a transaction.
   */
  function states($state = NULL) {
    // TODO: static
    $states = array(
      'pending' => array(
        'title' => t('Pending'),
        'description' => t('The transaction has been created.'),
      ),
      'complete' => array(
        'title' => t('Complete'),
        'description' => t('The transaction has been completed successfully.'),
      ),
      'canceled' => array(
        'title' => t('Canceled'),
        'description' => t('The transaction has been cancelled.'),
      ),
      'refunded' => array(
        'title' => t('Refunded'),
        'description' => t('The transaction has been refunded.'),
      ),
      'validated' => array(
        'title' => t('Validated'),
        'description' => t('The transaction has been validated and is ready for processing'),
      ),
      'error' => array(
        'title' => t('Error'),
        'description' => t('The transaction has an error and cannot be processed')
      ),
    );
    drupal_alter('tycoon_states', $states);
    
    if (isset($states[$state])) {
      return $states[$state];
    }
    
    return $states;
  }

  /**
   * Set the state for this transaction.
   */
  function set_state($state = 'pending') {
    if (array_key_exists($state, $this->states())) {
      $this->state = $state;
      $this->status = $state;
    }
  }

  /**
   * Return the current state for this transaction.
   */
  function state() {
    if (!$this->state) {
      $this->set_state();
    }
    return $this->state;
  }

  /**
   * Updates the total of the transaction.
   */
  protected function update_total() {
    $total = 0;
    foreach ($this->line_item as $item) {
      $total += $item->total;
    }
    $this->total = $total;
  }
  
  /**
   * A simple and easy way to call and run a transaction
   */
  function execute(&$data = NULL) {
    // Should we specify an action here rather than hardcoding????
    $method = array(
      'pending' => 'validate',
      'validated' => 'process',
    );
    while (in_array($this->state(), array('pending', 'validated'))) {
      $state = $this->state();
      $action = $method[$state];
      $this->do_action($action, $data);
    }
  }
  
  /**
   * TOKENS
   */
  function token_info() {
    $types['tycoon_transaction'] = TycoonTransaction::token_type_info();
    $tokens['tycoon_transaction'] = TycoonTransaction::token_token_info();
    
    $types['tycoon_pay_method'] = call_user_func(get_class(pay_method()) . '::token_type_info');
    $tokens['tycoon_pay_method'] = call_user_func(get_class(pay_method()) . '::token_token_info');
    
    foreach ($this->db_objects() as $type => $class_name) {
      $types['tycoon_' . $type] = call_user_func($class_name . '::token_type_info');
      $tokens['tycoon_' . $type] = call_user_func($class_name . '::token_token_info');
    }
    return array(
      'types' => $types,
      'tokens' => $tokens,
    );
  }
  
  static function token_type_info() {
    return array(
        'name' => t('Transaction'),
        'description' => t('Tokens related to Tycoon transactions.'),
        'needs-data' => 'tycoon_transaction',
    );
  }
  
  static function token_token_info() {
    return array(
      'txid' => array(
        'name' => t("Transaction ID"),
        'description' => t("The ID for this transaction."),
      ),
      'state' => array(
        'name' => t("State"),
        'description' => t("The current state for this transaction."),
      ),  
      'total' => array(
        'name' => t("Total"),
        'description' => t("The total for this transaction."),
      ),  
      'total-paid' => array(
        'name' => t("Total Paid"),
        'description' => t("The overall total paid for this transaction."),
      ),  
      'currency' => array(
        'name' => t("Currency"),
        'description' => t("The currency type for this transaction."),
      ),  
      'uid' => array(
        'name' => t("UID"),
        'description' => t("The user's ID for this transaction."),
      ),  
      'mail' => array(
        'name' => t("Email"),
        'description' => t("The user's email for this transaction."),
      ),  
      'first-name' => array(
        'name' => t("First Name"),
        'description' => t("The user's first name for this transaction."),
      ),  
      'last-name' => array(
        'name' => t("Last Name"),
        'description' => t("The user's last name for this transaction."),
      ),  
      'full-name' => array(
        'name' => t("Full Name"),
        'description' => t("The user's full name for this transaction."),
      ),  
      'phone' => array(
        'name' => t("Phone"),
        'description' => t("The user's phone number for this transaction."),
      ),  
      'company' => array(
        'name' => t("Company"),
        'description' => t("The user's company for this transaction."),
      ),  
      'street1' => array(
        'name' => t("Street Address 1"),
        'description' => t("The user's first line street address for this transaction."),
      ),  
      'street2' => array(
        'name' => t("Street Address 2"),
        'description' => t("The user's first line street address for this transaction."),
      ),  
      'city' => array(
        'name' => t("City"),
        'description' => t("The user's city for this transaction."),
      ),  
      'address' => array(
        'name' => t("Full Address"),
        'description' => t("The user's full address for this transaction."),
      ),   
      'province' => array(
        'name' => t("State / Province"),
        'description' => t("The user's state/province for this transaction."),
      ),   
      'postal-code' => array(
        'name' => t("Postal Code"),
        'description' => t("The user's postal code for this transaction."),
      ),   
      'country' => array(
        'name' => t("Country"),
        'description' => t("The user's country for this transaction."),
      ),   
      'created' => array(
        'name' => t("Created Date"),
        'description' => t("The creation date for this transaction."),
      ),   
      'updated' => array(
        'name' => t("Updated Date"),
        'description' => t("The updated date for this transaction."),
      ),    
      'data' => array(
        'name' => t("Data"),
        'description' => t("The data for this transaction."),
      ),       
      'line-items' => array(
        'name' => t("Line Items"),
        'description' => t("The line items for this transaction."),
      ),  
          
    );
  }
  
  function tokens($tokens, $options = array()) {
    $url_options = array('absolute' => TRUE);
    if (isset($options['language'])) {
      $url_options['language'] = $options['language'];
      $language_code = $options['language']->language;
    }
    else {
      $language_code = NULL;
    }
    $sanitize = !empty($options['sanitize']);

    $replacements = array();
    
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the transaction.
        case 'txid':
          $replacements[$original] = $this->txid;
          break;
        case 'state':
          $replacements[$original] = $this->state;
          break;
        case 'total':
          $replacements[$original] = $this->total;
          break;
        case 'total-paid':
          $replacements[$original] = $this->total_paid;
          break;
        case 'currency':
          $replacements[$original] = $this->currency;
          break;
        case 'uid':
          $replacements[$original] = $this->uid;
          break;
        case 'mail':
          $replacements[$original] = $this->mail;
          break;
        case 'first-name':
          $replacements[$original] = $this->first_name;
          break;
        case 'last-name':
          $replacements[$original] = $this->last_name;
          break;
        case 'full-name':
          $replacements[$original] = $this->first_name . ' ' . $this->last_name;
          break;          
        case 'phone':
          $replacements[$original] = $this->phone;
          break;  
        case 'company':
          $replacements[$original] = $this->company;
          break;  
        case 'street1':
          $replacements[$original] = $this->street1;
          break;  
        case 'street2':
          $replacements[$original] = $this->street2;
          break;  
        case 'city':
          $replacements[$original] = $this->city;
          break; 
        case 'address':
          $replacements[$original] = $this->street1 . ', ' . $this->street2 . ', ' . $this->city . ', ' . $this->province . ', ' . $this->postal_code;
          break;            
        case 'province':
          $replacements[$original] = $this->province;
          break;    
        case 'postal-code':
          $replacements[$original] = $this->postal_code;
          break;    
        case 'country':
          $replacements[$original] = $this->country;
          break;    
        case 'created':
          $replacements[$original] = $this->created;
          break;    
        case 'updated':
          $replacements[$original] = $this->updated;
          break;    
        case 'data':
          $replacements[$original] = $this->data;
          break;  
        
        case 'line-items':
          $output = '';
          foreach ($this->line_item as $line_item) {
            $output .= token_replace(t('[tycoon_line_item:title] x [tycoon_line_item:qty] = [tycoon_line_item:total]'), array('tycoon_line_item' => $line_item)) . '<br />';
          }
          $replacements[$original] = $output;
          break;
      }
    }
    
    return $replacements;
  }
}

/**
 * Base class for tycoon's database objects.
 */
class TycoonDBObject {
  /**
   * Initialize this object, setting values from schema defaults.
   *
   * @param $init
   *   If TRUE values will be filled in from schema defaults.
   */
  function init($init = TRUE) {
    if (is_array($init)) {
      return $this->load_row($init);
    }

    if (!$init) {
      return;
    }

    $schema = drupal_get_schema($this->db_table);

    if (!$schema) {
      return;
    }

    // Go through our schema and build correlations.
    foreach ($schema['fields'] as $field => $info) {
      if ($info['type'] == 'serial') {
        $this->$field = NULL;
      }
      if (!isset($this->$field)) {
        if (!empty($info['serialize']) && isset($info['serialized default'])) {
          $this->$field = unserialize($info['serialized default']);
        }
        elseif (isset($info['default'])) {
          $this->$field = $info['default'];
        }
        else {
          $this->$field = '';
        }
      }
    }
  }

  /**
   * Write the row to the database.
   *
   * @param $update
   *   If true this will be an UPDATE query. Otherwise it will be an INSERT.
   */
  function save_row($update = NULL) {
    $fields = $defs = $values = $serials = array();
    $schema = drupal_get_schema($this->db_table);

    // Go through our schema and build correlations.
    foreach ($schema['fields'] as $field => $info) {
      // special case -- skip serial types if we are updating.
      if ($info['type'] == 'serial') {
        $serials[] = $field;
        continue;
      }
      elseif ($info['type'] == 'int') {
        $this->$field = (int) $this->$field;
      }
      $fields[$field] = empty($info['serialize']) ? $this->$field : serialize($this->$field);
    }
    if (!$update) {
      $query = db_insert($this->db_table);
    }
    else {
      $query = db_update($this->db_table)
        ->condition($update, $this->$update);
    }
    $return = $query
      ->fields($fields)
      ->execute();

    if ($serials && !$update) {
      // get last insert ids and fill them in.
      // Well, one ID.
      foreach ($serials as $field) {
        $this->$field = $return;
      }
    }
    
    return $this->{$this->db_key};
  }

  /**
   * Load the object with a row from the database.
   *
   * This method is separate from the constructor in order to give us
   * more flexibility in terms of how the object is built in different
   * contexts.
   *
   * @param $data
   *   An object from db_fetch_object. It should contain all of the fields
   *   that are in the schema.
   */
  function load_row($data) {
    $schema = drupal_get_schema($this->db_table);

    // Go through our schema and build correlations.
    foreach ($schema['fields'] as $field => $info) {
      $this->$field = empty($info['serialize']) ? $data->$field : unserialize($data->$field);
    }
    
    //Assign the db_key value to the id parameter of the object
    $id = $this->db_key;
    $this->id = $this->$id;
  }
  
  /**
   * TOKENS
   */
  static function token_type_info() { }
  
  static function token_token_info() { }
  
  function tokens($tokens, $options = array()) { }
}
