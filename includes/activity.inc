<?php
/**
 * @file activity.inc
 * Provides the TycoonActivity object type and associated methods.
 */

/**
 * An object that contains all data of a single activity.
 */
class TycoonActivity extends TycoonDBObject {
  protected $db_table = 'tycoon_activity';
  protected $db_key = 'aid';
  var $hook = 'action';

  function TycoonActivity($init = TRUE) {
    parent::init($init);
    if ($init) {
      $this->created = time();
    }
  }

  function options($args = array()) {
    $keys = array('action', 'message', 'args', 'data', 'txid', 'status');
    foreach ($args as $key => $value) {
      if (in_array($key, $keys)) {
        $this->$key = $value;
      }
    }
  }

  static function token_type_info() {
    return array(
      'name' => t('Activity'),
      'description' => t('Tokens related to Tycoon activity.'),
      'needs-data' => 'tycoon_activity',
    );
  }
  
  static function token_token_info() {
    return array(
      'action' => array(
        'name' => t("Action"),
        'description' => t("The action of this activity."),
      ),
      'message' => array(
        'name' => t("Message"),
        'description' => t("The message for this activity."),
      ),  
      'args' => array(
        'name' => t("Arguments"),
        'description' => t("The arguements for this activity."),
      ),  
      'data' => array(
        'name' => t("Data"),
        'description' => t("The data of this activity."),
      ),  
      'txid' => array(
        'name' => t("Transaction ID"),
        'description' => t("The transaction ID of this activity."),
      ),  
      'status' => array(
        'name' => t("Status"),
        'description' => t("The status this activity."),
      ),         
    );
  }
  
  function tokens($tokens, $options = array()) {
    $url_options = array('absolute' => TRUE);
    if (isset($options['language'])) {
      $url_options['language'] = $options['language'];
      $language_code = $options['language']->language;
    }
    else {
      $language_code = NULL;
    }
    $sanitize = !empty($options['sanitize']);

    $replacements = array();
        
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'action':
          $replacements[$original] = $this->action;
          break;
        case 'message':
          $replacements[$original] = $this->message;
          break; 
        case 'args':
          $replacements[$original] = $this->args;
          break; 
        case 'data':
          $replacements[$original] = $this->data;
          break; 
        case 'txid':
          $replacements[$original] = $this->txid;
          break;           
        case 'status':
          $replacements[$original] = $this->status;
          break;                                                                                        
      }
    }
    
    return $replacements;
  }
}
