<?php

class TycoonActionException extends Exception {
  var $args = array();
  var $errors = array();
  
  public function __construct($message, $args = array(), $errors = array(), $code = 0) {
    $this->args = $args;
    $this->errors = $errors;

    // make sure everything is assigned properly
    parent::__construct($message, $code);
  }

  // custom string representation of object
  public function __toString() {
    return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  }

  public function activity() {
    return array(
      'message' => $this->message,
      'args' => $this->args,
      'data' => $this->errors,
    );
  }
}
