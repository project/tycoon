<?php
/**
 * A default Anction handler. All Actions should extend this class
 */
class TycoonActionHandler {
  //At this point this whole handler is really just a place holder
  //As we see the need for extra functionality to be built into it
  //we'll add to the base set of methods and properties
  
  protected $key = 'base';
  protected $msg = array();
  protected $status = TYCOON_ACTION_FAILED;
  
  /**
   * Constructor function for the Action Handler
   * All this function does is call the init method
   * 
   * This allows child classes to not have to override
   * the __construct methond and only implement the init method if needed
   * They must still call parent::init() in their class
   * if they want the parent init function to run
   */
  function __construct($transaction) {
    $this->init($transaction);
  }
  
  function init($transaction) {
    $this->txn = $transaction;
  }
  
  function execute(&$args) {
    try {
      //This is where you would call your own action logic
      $activity = array();
      $this->log_activity($activity + array('args' => array(), 'data' => array()));
    }
    catch (TycoonActionException $e) {
      $this->set_errors($e->errors);
      $this->log_activity($e->activity());
    }
  }
  
  /**
   * Sets an Action message that can be used by the calling function
   * 
   * @param $message
   *   The message to set. This should already be wrapped in t() if it is
   *   expected to be shown to the ed user.
   * @param $type
   *   The type of message. For error messages use the set_error() method. Defaults to 'status'.
   */
  function set_msg($message, $type = 'status') {
    $this->msg[$type] = $message;
  }
  
  /**
   * Sets an error message in the action
   * 
   * @param $message
   *   The message the error provides
   * @param ...
   *   Any additional data the error provides
   */
  function set_errors($errors) {
    foreach ($errors as $error) {
      $this->msg['error'][] = $error + array('args' => array(), 'data' => array());
    }
  }
  
  /**
   * Returns the errors array
   */
  function errors($reset = TRUE) {
    $errors = $this->messages('error', $reset);
    return $errors;
  }
  
  /**
   * Returns the Messages array and by default resets it after returning it.
   * 
   * @param $type
   *   Defaults to all message types
   * 
   * @param $reset
   *   Weather or not to reset the messages array. We do this by default because it is assumed that
   *   if a user is retrieving these then any subsequent messages should be add to a blank array
   *   so they are easy to distinguish.
   */
  function messages($type = NULL, $reset = TRUE) {
    $msgs = $this->msg;
    if ($type && isset($msgs[$type])) {
      $msgs = $msgs[$type];
      unset($this->msg[$type]);
    }
    else {
      $this->msg = array();
    }
    return $msgs;
  }
  
  /**
   * Logs activity to the database.
   * 
   * @param $activity
   *   An activity array.
   */
  function log_activity($activity) {
    $activity += array($args = array(), $data = array());
    $activity['action'] = $this->key;
    $activity['status'] = $this->status;
    $aid = $this->txn->log_activity($activity);
    return $aid;
  }
  
  /**
   * Returns the status of the current Action
   * 
   * @return
   *   The status of the current action.
   */
  function status() {
    return $this->status;
  }
  
  /**
   * Sets the status of the taken action.
   * 
   * Should be called within the execute() method
   * to set a valid status for the action taken.
   * 
   * If the action failed simply set the status to TYCOON_ACTION_FAILED
   * If it was successful then set it to TYCOON_ACTION_SUCCES
   * 
   * The set_msg and set_error methods can also be used to describe the current status
   */
  function set_status($status) {
    $this->status = $status;
  }
}
