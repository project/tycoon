<?php
/**
 * @file line_item.inc
 * Provides the TycoonLineItem object type and associated methods.
 */

/**
 * An object that contains all data of a single line item.
 */
class TycoonLineItem extends TycoonDBObject {
  var $db_table = 'tycoon_line_item';
  var $db_key = 'lid';

  function TycoonLineItem($init = TRUE) {
    parent::init($init);
  }

  function options($args = array()) {
    $keys = array('title', 'price', 'taxable', 'qty', 'description', 'weight', 'data', 'txid');
    foreach ($args as $key => $value) {
      if (in_array($key, $keys)) {
        $this->$key = $value;
      }
    }
    $this->total = ($this->price * $this->qty);
  }
  
  static function token_type_info() {
    return array(
      'name' => t('Line Items'),
      'description' => t('Tokens related to Tycoon line items.'),
      'needs-data' => 'tycoon_line_item',
    );
  }
  static function token_token_info() {
    return array(
      'title' => array(
        'name' => t("Title"),
        'description' => t("The title of this transaction."),
      ),
      'price' => array(
        'name' => t("Price"),
        'description' => t("The price of this line item."),
      ),  
      'taxable' => array(
        'name' => t("Taxable"),
        'description' => t("The taxability of this line item."),
      ),  
      'qty' => array(
        'name' => t("Quantity"),
        'description' => t("The quantity of this line item."),
      ),  
      'description' => array(
        'name' => t("Description"),
        'description' => t("The description of this line item."),
      ),  
      'weight' => array(
        'name' => t("Weight"),
        'description' => t("The weight of this line item."),
      ),  
      'data' => array(
        'name' => t("Data"),
        'description' => t("The data of this line item."),
      ),  
      'txid' => array(
        'name' => t("Transaction ID"),
        'description' => t("The transaction ID of this line item."),
      ),  
      'total' => array(
        'name' => t("Total"),
        'description' => t("The total cost of this line item."),
      ),  
         
    );
  }
  
  function tokens($tokens, $options = array()) {
    $url_options = array('absolute' => TRUE);
    if (isset($options['language'])) {
      $url_options['language'] = $options['language'];
      $language_code = $options['language']->language;
    }
    else {
      $language_code = NULL;
    }
    $sanitize = !empty($options['sanitize']);

    $replacements = array();
    
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'title':
          $replacements[$original] = $this->title;
          break; 
        case 'price':
          $replacements[$original] = $this->price;
          break; 
        case 'taxable':
          $replacements[$original] = $this->taxable;
          break;
        case 'qty':
          $replacements[$original] = $this->qty;
          break; 
        case 'description':
          $replacements[$original] = $this->description;
          break; 
        case 'weight':
          $replacements[$original] = $this->weight;
          break;   
        case 'data':
          $replacements[$original] = $this->data;
          break;   
        case 'txid':
          $replacements[$original] = $this->txid;
          break;  
       case 'total':
          $replacements[$original] = $this->price * $this->qty;
          break;                                                                                        
      }
    }
    
    return $replacements;
  }
}
