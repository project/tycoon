<?php
/**
 * @file pay_method.inc
 * Provides the TycoonPayMethod object type and associated methods.
 */


/**
 * Interface for pay method classes.
 */
interface TycoonPayMethodInterface {

  function init($options);
  
  /**
   * Provide a form to edit options for this pay method.
   *
   * @param $form
   *   The form for manipulation.
   * @param $form_state
   *   The form state for manipulation.
   */
  function options_form(&$form, &$form_state);

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state);

  /**
   * Any special handling on the options form.
   */
  function options_submit(&$form, &$form_state);
  
  /**
   * Validates schema fields.
   *
   * @param $transaction
   *   The transaction object.
   * @param $data
   *   The data to be validated against the schema.
   *
   * @return
   *   TRUE on success.
   *   Array of errors on failure.
   */
  function validate(&$transaction, &$data);
  
  /**
   * Submits pay method. Results in transaction complete state.
   *
   * @param $transaction
   *   The transaction object.
   * @param $data
   *   The data to be processed.
   *
   * @return
   *   TRUE on success.
   *   Array of errors on failure.
   */
  function process(&$transaction, &$data);
  
  /**
   * Submits pay method. Results in transaction complete state.
   *
   * @param $aid
   *   The activity ID.
   * @param $data
   *   The data to be saved.
   */
  function save($aid, $data);

  /**
   * Returns schema information for pay method.
   *
   * @return
   *   Schema array.
   */
  function get_schema();
  
  function destroy();
}

/**
 * An object that represents a pay method.
 */
class TycoonPayMethod implements TycoonPayMethodInterface {
  
  /**
   * Options for the object.
   */
  var $options = array();
  
  /**
   * Constructor
   */
  function _construct($options = array()) {
    $this->init($options);
  }
  
  function init($options) {
    $this->set_default_options();
    
    $this->unpack_options($options);
  }
  
  /**
   * Information about options for all kinds of purposes will be held here.
   */
  protected function option_definition() {
    $options['status'] = array('default' => FALSE);
    $options['weight'] = array('default' => 0);
    
    return $options;
  }

  /**
   * Set default options.
   */
  protected function set_default_options() {
    $this->_set_option_defaults($this->option_definition());
  }
  
  private function _set_option_defaults($options) {
    foreach ($options as $option => $definition) {
      $this->options[$option] = isset($definition['default']) ? $definition['default'] : NULL;
    }
  }
  
  protected function unpack_options($options) {
    if (!is_array($options)) {
      return;
    }
    
    $this->options = $options + $this->options;
  }
  
  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    $form['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => !empty($this->options['status']),
    );
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) { }

  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) { }


  /**
   * Checks data types and required.
   */
  function validate(&$transaction, &$data) {
    $errors = array();
    
    // Check required fields
    foreach ($this->get_schema() as $key => $value) {
      // Set transaction data
      $transaction->$key = isset($data[$key]) ? $data[$key] : NULL;
      if ($value['required'] && empty($data[$key])) {
        $errors[] = array(
          'message' => '%title field required.',
          'args' => array('%title' => $value['title']),
          'data' => array(
            'keys' => array($key),
          ),
        );
      }
    }
    
    if (!empty($errors)) {
      throw new TycoonActionException('Pay method transaction failed.', array(), $errors);
    }
    
    return TRUE;
  }

  function process(&$transaction, &$data) {
    // Do nothing
    return TRUE;
  }
  
  function save($aid, $data) {
    // Do nothing
  }
  
  protected function schema() {
    return array();
  }
  
  function get_schema() {
    static $schema;
    
    if (!$schema) {
      $schema = $this->schema();
      foreach ($schema as $key => &$value) {
        $value += array(
          'title' => '',
          'description' => '',
          'required' => FALSE,
          //'maxlength' => FALSE,
          //'weight' => 0,
        );
      }
    }
    
    // Let other modules alter the pay method schema definition.
    // Where to put this?
    // drupal_alter('tycoon_pay_method_schema', $id, $schema);
    
    return $schema;
  }
  
  function destroy() {
    if (isset($this->transaction)) {
      unset($this->transaction);
    }
  }
  
  static function token_type_info() {
    return array(
      'name' => t('Pay Method'),
      'description' => t('Tokens related to Tycoon pay method.'),
      'needs-data' => 'tycoon_pay_method',
    );
  }
  
  static function token_token_info() {
    return array(
      'data' => array(
        'name' => t("Data"),
        'description' => t("The data of this pay method."),
      ),
    );   
  }
  
  function tokens($tokens, $options = array()) {
    $url_options = array('absolute' => TRUE);
    if (isset($options['language'])) {
      $url_options['language'] = $options['language'];
      $language_code = $options['language']->language;
    }
    else {
      $language_code = NULL;
    }
    $sanitize = !empty($options['sanitize']);

    $replacements = array();
        
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'data':
          $replacements[$original] = $this->data;
          break;                                                                                      
      }
    }
    
    return $replacements;
  }
}
