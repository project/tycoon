<?php

/**
 * @file
 * Administration and module settings UI.
 */

function tycoon_admin_form($form) {
  
  return system_settings_form($form);
}

function tycoon_pay_method_admin_form($form) {
  $pay_methods = tycoon_pay_method_get_info();

  $form['pay_methods'] = array(
    '#theme' => 'tycoon_pay_method_admin_form_pay_methods',
    '#tree' => TRUE,
  );  
  uasort($pay_methods, '_tycoon_weight_sort');

  foreach ($pay_methods as $method_name => $pay_method) {
    $form['pay_methods'][$method_name]['label'] = array(
      '#markup' => $pay_method['label'] . '<br /><span style="font-size:0.85em;">' . $pay_method['description'] . '</span>',
    );
    $form['pay_methods'][$method_name]['status'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $pay_method['status'],
    );
    $form['pay_methods'][$method_name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $pay_method['label'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $pay_method['weight'],
    );
    $form['pay_methods'][$method_name]['operations'] = array(
      '#markup' => l(t('edit'), 'admin/config/tycoon/pay-methods/' . $method_name . '/edit'),
    );
  }

  $form = system_settings_form($form);
  $form['#submit'] = array('tycoon_pay_method_admin_form_submit');
  
  return $form;
}

function tycoon_pay_method_admin_form_submit($form, &$form_state) {
  $pay_methods = $form_state['values']['pay_methods'];
  foreach ($pay_methods as $method_name => $options) {
    $method = tycoon_get_pay_method($method_name);
    tycoon_save_pay_method_options($method_name, $options);
  }
  
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Returns HTML for the pay method admin form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_tycoon_pay_method_admin_form_pay_methods($variables) {
  $form = $variables['form'];

  drupal_add_tabledrag('pay-method-admin-table', 'order', 'sibling', 'method-weight');

  $rows = array();
  $headers = array(t('Method'), t('Weight'), t('Enabled'), t('Default'), t('Operations'));

  $delta = 0;
  foreach (element_children($form) as $key) {
    // Set special classes for drag and drop updating.
    $form[$key]['weight']['#attributes']['class'] = array('method-weight');

    // Build the table row.
    $data = array(
      drupal_render($form[$key]['label']),
      drupal_render($form[$key]['weight']),
      drupal_render($form[$key]['status']),
      '<div class="pay-method-default">' . ($delta++ == 0 ? t('Yes') : '') . '</div>',
      drupal_render($form[$key]['operations']),
    );
    $row = array('data' => $data, 'class' => array('draggable'));

    // Add any additional classes set on the row.
    if (!empty($form[$key]['#attributes']['class'])) {
      $row['class'] = array_merge($row['class'], $form[$key]['#attributes']['class']);
    }

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array('id' => 'pay-method-admin-table')));
  $output .= drupal_render_children($form);
  return $output;
}

function tycoon_pay_method_options_form(&$form, &$form_state, $method_name) {
  $method_info = tycoon_pay_method_get_info($method_name);
  
  if (!$method_info) {
    drupal_access_denied();
  }
  
  drupal_set_title(t('Pay Method @label', array('@label' => $method_info['label'])));
  
  $form['method'] = array(
    '#type' => 'value',
    '#value' => $method_name,
  );
  
  $method = tycoon_get_pay_method($method_name);
  $method->options_form($form, $form_state);
  
  $form = system_settings_form($form);
  $form['#validate'] = array('tycoon_pay_method_options_form_validate');
  $form['#submit'] = array('tycoon_pay_method_options_form_submit');
  
  return $form;
}

function tycoon_pay_method_options_form_validate(&$form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  $method = tycoon_get_pay_method($form_state['values']['method']);
  $method->options_validate($form, $form_state);
}

function tycoon_pay_method_options_form_submit(&$form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  $method_name = $form_state['values']['method'];
  
  $method = tycoon_get_pay_method($method_name);
  $method->options_submit($form, $form_state);
  
  $options = $form_state['values'];
  unset($options['method']);
  
  tycoon_save_pay_method_options($method_name, $options);
  
  $form_state['redirect'] = 'admin/config/tycoon/pay-methods';
  
  drupal_set_message(t('The configuration options have been saved.'));
}
